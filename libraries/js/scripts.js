$(function() {
          // When the toggle areas in your navbar are clicked, toggle them
          $("#search-button, #search-icon").click(function(e) {
              e.preventDefault();
              $("#search-button, #search-form").toggle();
          });
        });

      $(window).scroll(function() {
          if ($(this).scrollTop() <= 1050) { /* match parallax height. other option: parallax ht - header row height */
              $('nav').css('background', 'rgba(0,0,0,0.6)');
          } else {
              $('nav').css('background', 'rgba(0,0,0,0.9)');
          }
      });